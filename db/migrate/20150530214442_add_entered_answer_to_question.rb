class AddEnteredAnswerToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :entered_answer, :string
  end
end
