class Question < ActiveRecord::Base
  validates_presence_of :question
  validates_presence_of :answer
  validate :correct_answer, if: :answered?

  def correct_answer
    unless is_correct?
      errors.add(:answer, "is incorrect.")
    end
  end

  def answered?
  	!entered_answer.blank?
  end

  def is_correct?(submission = entered_answer)
    #all numbers are converted to words
    a = answer.gsub(/\d+/) { |num| num.to_i.humanize }
    s = submission.gsub(/\d+/) { |num| num.to_i.humanize }

    #then compare ignoring whitespaces and letter case
    a.downcase.strip.squeeze(' ') == s.downcase.strip.squeeze(' ')
  end
end
