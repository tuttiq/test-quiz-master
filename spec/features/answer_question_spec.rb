feature "Answer question", :js => true, :driver => :poltergeist do
  given(:question) { Question.create(question: "Test: What's the meaning of life?", answer: "42") }

  scenario "User answers a question correctly" do
    visit "/questions/" + question.id.to_s

    fill_in "answer", :with => question.answer
    click_button "Submit answer" 

    expect(page).to have_text("Congratulations! Your answer is right!")
  end

  scenario "User answers a question incorrectly" do
    visit "/questions/" + question.id.to_s

    fill_in "answer", :with => question.answer + "wrong answer"
    click_button "Submit answer" 

    expect(page).to have_text("Your answer is wrong, try again...")
  end
end